<?php
namespace ThibaudDauce\MattermostLogger;
use Monolog\Logger;
use ThibaudDauce\Mattermost\Mattermost;
use Monolog\Handler\AbstractProcessingHandler;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;

class MattermostHandler extends AbstractProcessingHandler
{
    public function __construct(Mattermost $mattermost, $options = [])
    {
        $this->options = array_merge([
            'webhook' => null,
            'channel' => 'town-square',
            'icon_url' => null,
            'username' => 'Laravel Logs',
            'level' => Logger::INFO,
            'level_mention' => Logger::ERROR,
            'mentions' => ['@here'],
            'short_field_length' => 62,
            'max_attachment_length' => 6000,
        ], $options);

        $this->mattermost = $mattermost;
    }

    public function write(array $record): void
    {
        if ($record['level'] < $this->options['level']) {
            return;
        }

        $message = Message::fromArrayAndOptions($record, $this->options);
        //catch the guzzle errors...
        try {
            $this->mattermost->send($message, $this->options['webhook']);
        } catch (ClientException $e) {
            Log::channel('daily')->error(Psr7\Message::toString($e->getRequest()));
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                Log::channel('daily')->error(Psr7\Message::toString($e->getRequest()));
            }
        } catch (\Exception $e) {
            if ($e->hasResponse()) {
                Log::channel('daily')->error($e->getResponse());
            } else {
                Log::channel('daily')->error($e->getMessage());
            }
        }
    }
}


